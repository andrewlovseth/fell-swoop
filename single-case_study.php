<?php

/*

	Single Case Study Template

*/

get_header(); ?>

	<?php get_template_part('template-parts/sections'); ?>

	<?php get_template_part('template-parts/case-study-pre-footer/case-study-pre-footer'); ?>

<?php get_footer(); ?>