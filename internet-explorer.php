<?php

/*

    Template Name: Internet Explorer

*/

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel='stylesheet' href='<?php bloginfo('template_directory'); ?>/style.css' type='text/css' media='all' />
</head>

<body>

    <div class="page ie-error">
        <h1>This site doesn't support Internet Explorer.</h1>

        <p>Please upgrade to a modern web browser.</p>

        <div class="options">
            <div class="option">
                <a href="https://www.microsoft.com/edge/" target="_blank">
                    <span class="icon">
                        <img src="<?php bloginfo('template_directory'); ?>/images/edge.png" alt="Edge Icon" />
                    </span>

                    <span class="name">
                        Microsoft Edge
                    </span>                    
                </a>
            </div>

            <div class="option">
                <a href="https://www.google.com/chrome/" target="_blank">
                    <span class="icon">
                        <img src="<?php bloginfo('template_directory'); ?>/images/chrome.png" alt="Chrome Icon" />
                    </span>

                    <span class="name">
                        Google Chrome
                    </span>                    
                </a>
            </div>


            <div class="option">
                <a href="https://mozilla.org/firefox/" target="_blank">
                    <span class="icon">
                        <img src="<?php bloginfo('template_directory'); ?>/images/firefox.png" alt="Firefox Icon" />
                    </span>

                    <span class="name">
                        Mozilla Firefox
                    </span>                    
                </a>
            </div>

            <div class="option">
                <a href="https://support.apple.com/downloads/safari" target="_blank">
                    <span class="icon">
                        <img src="<?php bloginfo('template_directory'); ?>/images/safari.png" alt="Safari Icon" />
                    </span>

                    <span class="name">
                        Apple Safari
                    </span>                    
                </a>
            </div>

        </div>
    </div>

</body>
</html>
