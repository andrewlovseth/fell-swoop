<?php

/*

	Services Module

*/

if( get_row_layout() == 'services' ): 

$theme = get_sub_field('theme');
$backgroundColor = get_sub_field('background_color');

$className = 'services grid';

if($theme) {
    $className .= ' ' . $theme;
}

if(get_row_index()) {
    $className .= ' row-' . get_row_index();
}

?>

	<section class="<?php echo esc_attr($className); ?>">

		<?php if($backgroundColor): ?>
			<style>
				@media only screen and (min-width:62em) { 
					.services.row-<?php echo get_row_index(); ?> {
						background: <?php echo $backgroundColor; ?>;
					}
				}
			</style>
		<?php endif; ?>

		<div class="content">
			<div class="services-header">
				<h3><?php the_sub_field('sub_header'); ?></h3>

				<h2><?php the_sub_field('headline'); ?></h2>
			</div>

			<?php if(have_rows('services')): ?>
				<div class="services-list">
					<?php while(have_rows('services')): the_row(); ?>

						<div class="service">
							<div class="icon">
								<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>

							<div class="info">
								<h4><?php the_sub_field('hed'); ?></h4>

								<div class="copy-medium">
									<p><?php the_sub_field('dek'); ?></p>
								</div>
							</div>
						</div>

					<?php endwhile; ?>
				</div>
			<?php endif; ?>			
		</div>
				
	</section>

<?php endif; ?>