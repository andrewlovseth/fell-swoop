<?php

/*

	Clients Module

*/

if( get_row_layout() == 'clients' ): 

$theme = get_sub_field('theme');

$className = 'clients grid';

if($theme) {
    $className .= ' ' . $theme;
}

if(get_row_index()) {
    $className .= ' row-' . get_row_index();
}

?>

	<section class="<?php echo esc_attr($className); ?>">

		<div class="clients-header">
			<h3><?php the_sub_field('header'); ?></h3>
		</div>

		<?php if(have_rows('clients')): ?>
			<div class="clients-list">
				<h2>
					<?php while(have_rows('clients')): the_row(); ?>

						<?php $case_study = get_sub_field('case_study'); if($case_study): ?>

							<span class="client has-link"><a href="<?php echo get_permalink($case_study->ID); ?>"><?php the_sub_field('name'); ?></a></span>

						<?php else: ?>
							
							<span class="client"><?php the_sub_field('name'); ?></span>

						<?php endif; ?> 		        

					<?php endwhile; ?>
				</h2>
			</div>
		<?php endif; ?>

	</section>

<?php endif; ?>