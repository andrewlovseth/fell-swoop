<?php

/*

	Case Study Pre Footer Module

*/

$case_studies = get_field('case_study_links', 'options');
$case_studies_count = count($case_studies);

$current_post = $post->ID;
$current_index = array_search($current_post, $case_studies);


// If 0 Index
if($current_index == 0) {

	$prev_index = $case_studies_count - 1;
	$next_index = $current_index + 1;
}

// If Last Index
elseif($current_index == $case_studies_count - 1) {

	$prev_index = $current_index - 1;
	$next_index = 0;

}

// All Others Inbetween
else {

	$prev_index = $current_index - 1;
	$next_index = $current_index + 1;
}

$prev_link = get_permalink($case_studies[$prev_index]);
$next_link = get_permalink($case_studies[$next_index]);


$className = 'case-study-pre-footer grid';

?>

<section class="<?php echo esc_attr($className); ?>">

	<div class="pagination">
		<div class="prev">
			<h3><a href="<?php echo $prev_link; ?>">Prev</a></h3>
		</div>

		<div class="next">
			<h3><a href="<?php echo $next_link; ?>">Next</a></h3>
		</div>
	</div>

	<div class="back-to-top">
		<h3><a href="#page" class="smooth top">Back to Top</a></h3>
	</div>

</section>