<?php

/*

	Case Study Hero Module

*/

if( get_row_layout() == 'case_study_hero' ): 

$theme = get_sub_field('theme');

$className = 'case-study-hero grid';

if($theme) {
    $className .= ' ' . $theme;
}

if(get_row_index()) {
    $className .= ' row-' . get_row_index();
}

?>

	<section class="<?php echo esc_attr($className); ?>">
		<div class="content">

			<div class="project-sub-headline">
				<div class="project-name">
					<h5><?php the_sub_field('project_name'); ?></h5>
				</div>

				<div class="separator"></div>

				<div class="project-type copy-medium">
					<p><?php the_sub_field('project_type'); ?></p>
				</div>
			</div>

			<div class="project-headline">
				<h1><?php the_sub_field('headline'); ?></h1>
			</div>

			<div class="toggle-link">
				<a href="#" class="project-description-trigger">Details +</a>
			</div>

			<div id="project-description" class="project-description copy-medium">
				<?php the_sub_field('copy'); ?>
			</div>

		</div>
	</section>

<?php endif; ?>