<?php

/*

	Home Hero Module

*/

if( get_row_layout() == 'home_hero' ): 

$theme = get_sub_field('theme');

$className = 'home-hero grid';

if($theme) {
    $className .= ' ' . $theme;
}

if(get_row_index()) {
    $className .= ' row-' . get_row_index();
}

?>

	<section class="<?php echo esc_attr($className); ?>">

		<div class="content" style="background-image: url(<?php $image = get_sub_field('background_graphic'); echo $image['url']; ?>);">

			<div class="flex-wrapper">
				<div class="headline">
					<h1><?php the_sub_field('headline'); ?></h1>
				</div>

				<div class="copy-large">
					<p><?php the_sub_field('sub_headline'); ?></p>
				</div>

				<div class="select-work">
					<h3><a href="#" class="home-smooth"><?php the_sub_field('select_work_label'); ?></a></h3>
				</div>					
			</div>					

		</div>

	</section>

<?php endif; ?>