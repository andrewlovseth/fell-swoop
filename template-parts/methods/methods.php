<?php

/*

	Methods Module

*/

if( get_row_layout() == 'methods' ): 

$theme = get_sub_field('theme');
$backgroundColor = get_sub_field('background_color');
$gutterColor = get_sub_field('gutter_color');


$className = 'methods grid';

if($theme) {
    $className .= ' ' . $theme;
}

if(get_row_index()) {
    $className .= ' row-' . get_row_index();
}

?>

	<section class="<?php echo esc_attr($className); ?>">

		<?php if($backgroundColor): ?>
			<style>
				.methods.row-<?php echo get_row_index(); ?>,
				.methods.row-<?php echo get_row_index(); ?> .border-bottom {
					background: <?php echo $backgroundColor; ?>;
				}
			</style>
		<?php endif; ?>

		<?php if($gutterColor): ?>
			<style>
				.methods.row-<?php echo get_row_index(); ?> .content:before,
				.methods.row-<?php echo get_row_index(); ?> .gutter {
					background: <?php echo $gutterColor; ?>;
				}
			</style>
		<?php endif; ?>

		<div class="content">
					
			<div class="methods-header">
				<h3><?php the_sub_field('sub_header'); ?></h3>

				<h2><?php the_sub_field('headline'); ?></h2>
			</div>

			<?php if(have_rows('methods')): ?>
				<div class="methods-list">
					<?php while(have_rows('methods')): the_row(); ?>

						<div class="method">
							<div class="copy-large headline">
								<p><?php the_sub_field('hed'); ?></p>
							</div>

							
							<div class="copy-large">
								<p><?php the_sub_field('dek'); ?></p>
							</div>
						</div>

					<?php endwhile; ?>
				</div>
			<?php endif; ?>

		</div>

		<div class="gutter"></div>
		<div class="border-bottom">
			<a href="#page" class="smooth top fs-link">Back to Top</a>
		</div>
	</section>

<?php endif; ?>