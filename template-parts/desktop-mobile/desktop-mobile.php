<?php

/*

	Desktop & Mobile Module

*/

if( get_row_layout() == 'desktop_mobile' ): 

$theme = get_sub_field('theme');
$sectionAlignment = get_sub_field('section_alignment');
$phoneGraphicAlignment = get_sub_field('phone_graphic_alignment');
$backgroundColor = get_sub_field('background_color');
$showCaseStudy = get_sub_field('show_case_study_info');
$case_study = get_sub_field('case_study');

$className = 'desktop-mobile grid';

if($theme) {
    $className .= ' ' . $theme;
}

if($sectionAlignment) {
    $className .= ' section-' . $sectionAlignment;
}

if($phoneGraphicAlignment) {
    $className .= ' phone-' . $phoneGraphicAlignment;
}

if(get_row_index()) {
    $className .= ' row-' . get_row_index();
}

if($showCaseStudy == true) {
	$className .= ' has-links';
}

if($showCaseStudy == false) {
	$className .= ' no-links';
}

?>

	<section class="<?php echo esc_attr($className); ?>">

		<?php if($backgroundColor): ?>
			<style>
				.desktop-mobile.row-<?php echo get_row_index(); ?>:before {
					background: <?php echo $backgroundColor; ?>;
				}
			</style>
		<?php endif; ?>

		<?php if($showCaseStudy == true): ?>

			<?php if(have_rows('sections', $case_study->ID)): while(have_rows('sections', $case_study->ID)) : the_row(); ?>

				<?php if( get_row_layout() == 'case_study_hero' ): ?>

						<div class="project-info copy-medium">
							<p>
								<a href="<?php echo get_permalink($case_study->ID); ?>">
									<strong><?php the_sub_field('project_name'); ?></strong>
									<?php the_sub_field('project_type'); ?>
								</a>
							</p>
						</div>

				<?php endif; ?>

			<?php endwhile; endif; ?>

		<?php endif; ?>

		<div class="screenshots">
			<div class="desktop">
				<?php 
					$desktop = get_sub_field('desktop_image'); 
					if($showCaseStudy == true): ?>

					<a href="<?php echo get_permalink($case_study->ID); ?>">

						<picture>
							<source media="(max-width: 599px)" srcset="<?php echo $desktop['sizes']['medium']; ?>">
							<source media="(min-width: 600px)" srcset="<?php echo $desktop['url']; ?>">
							<img src="<?php echo $desktop['url']; ?>" alt="<?php echo $desktop['alt']; ?>" />
						</picture>

					</a>

				<?php else : ?>
						
					<picture>
						<source media="(max-width: 599px)" srcset="<?php echo $desktop['sizes']['medium']; ?>">
						<source media="(min-width: 600px)" srcset="<?php echo $desktop['url']; ?>">
						<img src="<?php echo $desktop['url']; ?>" alt="<?php echo $desktop['alt']; ?>" />
					</picture>

				<?php endif; ?>
			</div>


			<div class="mobile">

				<?php
					$mobile = get_sub_field('mobile_image'); 

					if($showCaseStudy == true): ?>

					<a href="<?php echo get_permalink($case_study->ID); ?>">
						<div class="mobile-wrapper">
							<div class="image">
								<div class="content">

									<picture>
										<source media="(max-width: 599px)" srcset="<?php echo $mobile['sizes']['medium']; ?>">
										<source media="(min-width: 600px)" srcset="<?php echo $mobile['url']; ?>">
										<img src="<?php echo $mobile['url']; ?>" alt="<?php echo $mobile['alt']; ?>" />
									</picture>

								</div>						
							</div>
						</div>
					</a>

				<?php else: ?>

					<div class="mobile-wrapper">
						<div class="image">
							<div class="content">

								<picture>
									<source media="(max-width: 599px)" srcset="<?php echo $mobile['sizes']['medium']; ?>">
									<source media="(min-width: 600px)" srcset="<?php echo $mobile['url']; ?>">
									<img src="<?php echo $mobile['url']; ?>" alt="<?php echo $mobile['alt']; ?>" />
								</picture>
								
							</div>						
						</div>
					</div>
					
				<?php endif; ?>							

			</div>			
		</div>

	</section>

<?php endif; ?>