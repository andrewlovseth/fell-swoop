<?php if(have_rows('sections')): while(have_rows('sections')) : the_row(); ?>

	<?php include(locate_template('template-parts/home-hero/home-hero.php')); ?>

	<?php include(locate_template('template-parts/case-study-hero/case-study-hero.php')); ?>

	<?php include(locate_template('template-parts/aligned-image/aligned-image.php')); ?>

	<?php include(locate_template('template-parts/desktop-mobile/desktop-mobile.php')); ?>

	<?php include(locate_template('template-parts/inset-image/inset-image.php')); ?>

	<?php include(locate_template('template-parts/mobile-gallery/mobile-gallery.php')); ?>

	<?php include(locate_template('template-parts/full-bleed-gallery/full-bleed-gallery.php')); ?>

	<?php include(locate_template('template-parts/pullquote/pullquote.php')); ?>

	<?php include(locate_template('template-parts/clients/clients.php')); ?>

	<?php include(locate_template('template-parts/services/services.php')); ?>

	<?php include(locate_template('template-parts/methods/methods.php')); ?>

<?php endwhile; endif; ?>