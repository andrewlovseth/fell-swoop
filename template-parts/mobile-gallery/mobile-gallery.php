<?php

/*

	Mobile Gallery Module

*/

if( get_row_layout() == 'mobile_gallery' ): 

$theme = get_sub_field('theme');
$alignment = get_sub_field('alignment');
$backgroundColor = get_sub_field('background_color');
$showCaseStudy = get_sub_field('show_case_study_info');
$case_study = get_sub_field('case_study');

$className = 'mobile-gallery grid';

if($theme) {
    $className .= ' ' . $theme;
}

if($alignment) {
    $className .= ' section-' . $alignment;
}

if(get_row_index()) {
    $className .= ' row-' . get_row_index();
}

if($showCaseStudy == true) {
	$className .= ' has-links';
}

if($showCaseStudy == false) {
	$className .= ' no-links';
}

?>

	<section class="<?php echo esc_attr($className); ?>">

		<?php if($backgroundColor): ?>
			<style>
				.mobile-gallery.row-<?php echo get_row_index(); ?>:before {
					background: <?php echo $backgroundColor; ?>;
				}
			</style>
		<?php endif; ?>

		<?php if($showCaseStudy == true): ?>

			<?php if(have_rows('sections', $case_study->ID)): while(have_rows('sections', $case_study->ID)) : the_row(); ?>

				<?php if( get_row_layout() == 'case_study_hero' ): ?>

						<div class="project-info copy-medium">
							<p>
								<a href="<?php echo get_permalink($case_study->ID); ?>">
									<strong><?php the_sub_field('project_name'); ?></strong>
									<?php the_sub_field('project_type'); ?>
								</a>
							</p>
						</div>

				<?php endif; ?>

			<?php endwhile; endif; ?>

		<?php endif; ?>

		<div class="screenshots">

			<?php $images = get_sub_field('gallery'); if( $images ): ?>

				<?php foreach( $images as $image ): ?>

					<div class="mobile">
						<div class="mobile-wrapper">
							
							<div class="image">
								<div class="content">

									<?php if($showCaseStudy == true): ?>
										
										<a href="<?php echo get_permalink($case_study->ID); ?>">
											<picture>
												<source media="(max-width: 599px)" srcset="<?php echo $image['sizes']['medium']; ?>">
												<source media="(min-width: 600px)" srcset="<?php echo $image['url']; ?>">
												<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
											</picture>
										</a>

									<?php else : ?>
											
										<picture>
											<source media="(max-width: 599px)" srcset="<?php echo $image['sizes']['medium']; ?>">
											<source media="(min-width: 600px)" srcset="<?php echo $image['url']; ?>">
											<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
										</picture>
										
									<?php endif; ?>
									
								</div>
							</div>

						</div>
					</div>
					
				<?php endforeach; ?>

			<?php endif; ?>

		</div>

	</section>

<?php endif; ?>