<?php

/*

	Aligned Image Module

*/

if( get_row_layout() == 'aligned_image' ): 

$theme = get_sub_field('theme');
$alignment = get_sub_field('alignment');
$image = get_sub_field('image');
$mobile_image = get_sub_field('mobile_image');

$className = 'aligned-image grid';

if($theme) {
    $className .= ' ' . $theme;
}

if($alignment) {
    $className .= ' ' . $alignment;
}

if(get_row_index()) {
    $className .= ' row-' . get_row_index();
}

?>

	<section class="<?php echo esc_attr($className); ?>">

		<?php if($mobile_image): ?>

			<picture>
				<source media="(max-width: 599px)" srcset="<?php echo $mobile_image['url']; ?>">
				<source media="(min-width: 600px)" srcset="<?php echo $image['url']; ?>">
				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</picture>

		<?php else: ?>

			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

		<?php endif; ?>
		
	</section>

<?php endif; ?>