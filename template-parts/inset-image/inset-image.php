<?php

/*

	Inset Image Module

*/

if( get_row_layout() == 'inset_image' ): 

$theme = get_sub_field('theme');
$type = get_sub_field('type');
$alignment = get_sub_field('alignment');
$backgroundColor = get_sub_field('background_color');
$showCaseStudy = get_sub_field('show_case_study_info');
$case_study = get_sub_field('case_study');

$className = 'inset-image grid';

if($theme) {
    $className .= ' ' . $theme;
}

if($type) {
    $className .= ' type-' . $type;
}

if($alignment) {
    $className .= ' section-' . $alignment;
}

if(get_row_index()) {
    $className .= ' row-' . get_row_index();
}

if($showCaseStudy == true) {
	$className .= ' has-links';
}

if($showCaseStudy == false) {
	$className .= ' no-links';
}

?>

	<section class="<?php echo esc_attr($className); ?>">

		<?php if($backgroundColor): ?>
			<style>
				.inset-image.row-<?php echo get_row_index(); ?>:before {
					background: <?php echo $backgroundColor; ?>;
				}
			</style>
		<?php endif; ?>

		<?php if($showCaseStudy == true): ?>

			<?php if(have_rows('sections', $case_study->ID)): while(have_rows('sections', $case_study->ID)) : the_row(); ?>

				<?php if( get_row_layout() == 'case_study_hero' ): ?>

						<div class="project-info copy-medium">
							<p>
								<a href="<?php echo get_permalink($case_study->ID); ?>">
									<strong><?php the_sub_field('project_name'); ?></strong>
									<?php the_sub_field('project_type'); ?>
								</a>
							</p>
						</div>

				<?php endif; ?>

			<?php endwhile; endif; ?>

		<?php endif; ?>

		<div class="screenshot <?php echo $type; ?>">
			<div class="<?php echo $type; ?>-wrapper">
				
				<div class="image">
					<div class="content">

						<?php 
							$image = get_sub_field('image');
							if($showCaseStudy == true): ?>
							
							<a href="<?php echo get_permalink($case_study->ID); ?>">
								<picture>
									<source media="(max-width: 599px)" srcset="<?php echo $image['sizes']['medium']; ?>">
									<source media="(min-width: 600px)" srcset="<?php echo $image['url']; ?>">
									<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
								</picture>
							</a>

						<?php else : ?>
								
							<picture>
								<source media="(max-width: 599px)" srcset="<?php echo $image['sizes']['medium']; ?>">
								<source media="(min-width: 600px)" srcset="<?php echo $image['url']; ?>">
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</picture>

						<?php endif; ?>
						
					</div>
				</div>

			</div>		
		</div>

	</section>

<?php endif; ?>