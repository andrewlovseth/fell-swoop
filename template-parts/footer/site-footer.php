<div class="logo">
	<a href="<?php echo site_url('/'); ?>">
		<img src="<?php $image = get_field('footer_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	</a>
</div>

<div class="contact-info">
	<h3>
		<span class="contact-item email"><a href="mailto:<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a></span>

		<span class="contact-item phone"><a href="tel:<?php the_field('phone', 'options'); ?>"><?php the_field('phone', 'options'); ?></a></span>

		<span class="contact-item mailing-list">
			<?php 
				$link = get_field('mailing_list_link', 'options');
				if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
			 ?>

			 	<a href="#" class="subscribe-trigger"><?php echo esc_html($link_title); ?></a>

			<?php endif; ?>

			<div class="subscribe-form">
				<?php the_field('mailchimp_form', 'options'); ?>
			</div>
		</span>

	</h3>
</div>

<div class="copyright copy-small">
	<p><?php the_field('copyright', 'options'); ?></p>
</div>