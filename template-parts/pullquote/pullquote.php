<?php

/*

	Pullquote Module

*/

if( get_row_layout() == 'pullquote' ): 

$theme = get_sub_field('theme');

$className = 'pullquote grid';

if($theme) {
    $className .= ' ' . $theme;
}

if(get_row_index()) {
    $className .= ' row-' . get_row_index();
}

?>

	<section class="<?php echo esc_attr($className); ?>">
		
		<blockquote>
			<p><?php the_sub_field('quote'); ?></p>
		</blockquote>

		<div class="source copy-medium">
			<p>
				<?php if(get_sub_field('person')): ?>
					<strong><?php the_sub_field('person'); ?></strong>
				<?php endif; ?>	

				<?php if(get_sub_field('position')): ?>
					<?php the_sub_field('position'); ?>
				<?php endif; ?>	
			</p>
		</div>

	</section>

<?php endif; ?>