<?php

/*

	Full Bleed Gallery Module

*/

if( get_row_layout() == 'full_bleed_gallery' ): 

$images = get_sub_field('gallery'); 

$theme = get_sub_field('theme');
$type = get_sub_field('type');
$backgroundColor = get_sub_field('background_color');
$image_count = count($images);
$mobile_arrangement = get_sub_field('mobile_arrangement');


$className = 'full-bleed-gallery grid';

if($theme) {
    $className .= ' ' . $theme;
}

if($type) {
    $className .= ' type-' . $type;
}

if($image_count == 1) {
    $className .= ' single-image';
}

if($mobile_arrangement) {
    $className .= ' mobile-' . $mobile_arrangement;
}

if(get_row_index()) {
    $className .= ' row-' . get_row_index();
}

?>

	<section class="<?php echo esc_attr($className); ?>">

		<?php if($backgroundColor): ?>
			<style>
				.full-bleed-gallery.row-<?php echo get_row_index(); ?> {
					background: <?php echo $backgroundColor; ?>;
				}
			</style>
		<?php endif; ?>

		<div class="screenshots screenshots-<?php echo $type; ?>">

			<?php if( $images ): ?>

				<?php foreach( $images as $image ): ?>

					<div class="<?php echo $type; ?>">
						<div class="<?php echo $type; ?>-wrapper">
							
							<div class="image">
								<div class="content">

									<picture>
										<source media="(max-width: 599px)" srcset="<?php echo $image['sizes']['medium']; ?>">
										<source media="(min-width: 600px)" srcset="<?php echo $image['url']; ?>">
										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									</picture>
							
								</div>
							</div>

						</div>
					</div>
					
				<?php endforeach; ?>

			<?php endif; ?>

		</div>

	</section>

<?php endif; ?>