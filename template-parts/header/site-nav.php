<?php 

	$theme = get_field('navigation_theme');

	$className = 'site-nav grid';

	if($theme) {
	    $className .= ' ' . $theme;
	}

?>

<nav class="<?php echo esc_attr($className); ?>">
	<div class="content">
		
			
		<?php $posts = get_field('case_study_links', 'options'); if( $posts ): ?>
			<div class="case-study-links">
				<div class="nav-header">
					<h3>Case Studies</h3>
				</div>
				<?php foreach( $posts as $p ): ?>
					<div class="link">
						<a href="<?php echo get_permalink( $p ); ?>"><?php echo get_the_title( $p ); ?></a>
					</div>
				<?php endforeach; ?>		
			</div>
		<?php endif; ?>

		<div class="contact-info">
			<div class="nav-header">
				<h3>Connect</h3>
			</div>

			<div class="contact-details">
				<p>
					<span class="contact-item email"><a href="mailto:<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a></span>

					<span class="contact-item phone"><a href="tel:<?php the_field('phone', 'options'); ?>"><?php the_field('phone', 'options'); ?></a></span>

					<span class="contact-item mailing-list">
						<?php 
							$link = get_field('mailing_list_link', 'options');
							if( $link ): 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						 ?>

						 	<a href="#" class="subscribe-trigger"><?php echo esc_html($link_title); ?></a>

						<?php endif; ?>

						<div class="subscribe-form">
							<?php the_field('mailchimp_form', 'options'); ?>
						</div>
					</span>
				</p>				
			</div>
		</div>

	</div>

</nav>