<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<script>
		isIE11 = !!navigator.userAgent.match(/Trident.*rv\:11\./);
		if(isIE11)  {
	   		window.location = "<?php echo site_url('/internet-explorer/'); ?>";
		}
	</script>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div id="page" class="site">

	<?php 

		$theme = get_field('navigation_theme');
		$className = 'site-header grid';

		if($theme) {
		    $className .= ' ' . $theme;
		}

		$background_color = get_field('body_background_color'); 
	?>

	<?php if($background_color): ?>
		<style>
			body {
				background-color: <?php echo $background_color; ?>;
			}
		</style>

	<?php endif; ?>

	<header class="<?php echo esc_attr($className); ?>">

		<?php get_template_part('template-parts/header/site-logo'); ?>

		<?php get_template_part('template-parts/header/hamburger'); ?>

	</header>

	<?php get_template_part('template-parts/header/site-nav'); ?>

	<main class="site-content">	
