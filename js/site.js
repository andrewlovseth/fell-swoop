(function ($, window, document, undefined) {

	$(document).ready(function($) {

		// Nav Trigger
		$('.nav-trigger').on('click', function(){

			$('body').toggleClass('nav-open');	

			return false;
		});


		// Smooth Scroll
		$('.smooth').smoothScroll();

		// Home Smooth Scroll
		$('.home-smooth').on('click', function() {
			$.smoothScroll({
				scrollTarget: '.row-2'
			});

			return false;
		});


		// Toggle Case Study Project Description
		$(function(){
			var nav = $('#project-description'),
				animateTime = 300,
				navLink = $('.project-description-trigger');
		
			navLink.click(function(){
				if(nav.height() === 0){
					animateHeight(nav, animateTime);
				} else {
					nav.stop().animate({ height: '0' }, animateTime);
				}

				// Add 'open' class to section container
				$('.case-study-hero .content').toggleClass('open');

				// Toggle Text
				navLink.text(function(i, text){
					return text === "Details +" ? "Close —" : "Details +";
				});

				return false;

			});
		})

		// Animate Height
		function animateHeight(element, time){
			var curHeight = element.height(),
				autoHeight = element.css('height', 'auto').height();
		
			element.height(curHeight);
			element.stop().animate({ height: autoHeight }, time);
		}


		// Toggle Mailing List Form
		$('.subscribe-trigger').on('click', function(){

			$('.subscribe-trigger').hide();
			$('.subscribe-form').show();

			return false;
		});
			
	});


	// Escape
	$(document).keyup(function(e) {
		
		if (e.keyCode == 27) {
			$('body').removeClass('nav-open');
		}

	});


})(jQuery, window, document);